<?php 

include(dirname(__FILE__).'/PHP-Parser/lib/bootstrap.php');

use PhpParser\Error;
use PhpParser\ParserFactory;
$parser = new ParserFactory;
$parser = $parser->create(ParserFactory::PREFER_PHP7);

function get_all_php_files($directory) {
	//$time = microtime(true);
    $directory_stack = array($directory);
    $ignored_filename = array(
        '.git' => true,
        '.svn' => true,
        '.hg' => true,
        'index.php' => true,
    );
    $file_list = array();
    while ($directory_stack) {
        $current_directory = array_shift($directory_stack);
        $files = scandir($current_directory);
        foreach ($files as $filename) {
            //  Skip all files/directories with:
            //      - A starting '.'
            //      - A starting '_'
            //      - Ignore 'index.php' files
            $pathname = $current_directory . DIRECTORY_SEPARATOR . $filename;
            if (isset($filename[0]) && (
                $filename[0] === '.' ||
                $filename[0] === '_' ||
                isset($ignored_filename[$filename])
            )) 
            {
                continue;
            }
            else if (is_dir($pathname) === TRUE) {
                $directory_stack[] = $pathname;
            } else if (pathinfo($pathname, PATHINFO_EXTENSION) === 'php') {
                $file_list[] = $pathname;
            }
        }
    }
    //$time = microtime(true) - $time;
    //Debug::dump($time);
    return $file_list;
}

class Cache {
    public static function toFile($filepath, $data) {
    	$temp_path = dirname(__FILE__).'/cache';
        $path = $temp_path . '/' . static::sanitize($filepath);
        return file_put_contents($path, serialize($data));
    }

    public static function get($filepath) {
    	$temp_path = dirname(__FILE__).'/cache';
    	$path = $temp_path . '/' . static::sanitize($filepath);
    	if(file_exists($path)) {
            return unserialize(file_get_contents($path));
        }
        return null;
    }

    public static function sanitize($filepath) {
    	return str_replace(array('~', '.', '/', ' ', '!', "\n", "\r", "\t", '\\', ':', '\'', '"', ';'), '-', $filepath);
    }
}

class Debug {
	public static function dump($var) {
		echo '<pre>'.print_r($var, true).'</pre>';
	}
}

$files = get_all_php_files("D:\\wamp\\www\\Work\\DPCNSW\\dpc-nsw");
//$files = array_slice($files, 0, 5000);
$time = microtime(true);
$astFiles = array();
foreach ($files as $filepath) {
	try {
        $ast = null;
		//$ast = Cache::get($filepath);
		if ($ast === null) {
			$astFiles[] = $parser->parse(file_get_contents($filepath));
			//Cache::toFile($filepath, $ast);
		}
	} catch (Error $e) {
	    echo 'Parse Error: ', $e->getMessage();
	}
}
Debug::dump($astFiles); 
$time = microtime(true) - $time;
var_dump('Parsing time: '.$time.' seconds');